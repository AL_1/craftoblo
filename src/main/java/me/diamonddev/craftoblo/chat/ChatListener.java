package me.diamonddev.craftoblo.chat;

import me.diamonddev.craftoblo.playerdata.DataManager;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        String rank = "";
        if (e.getPlayer().isOp()) {
            rank = ChatColor.DARK_PURPLE + "*";
        }
        String prefix = "";
        try {
            prefix = DataManager.getUser(e.getPlayer()).getCurrentClass().getColor() + "";
        } catch (Exception ignored) {
        }
        String chatFormat = rank + ChatColor.RESET + prefix + "%s &8&l» &7%s";
        if (e.getPlayer().isOp() || e.getPlayer().hasPermission("Craftoblo.colorchat"))
            e.setMessage(ChatColor.translateAlternateColorCodes('&', e.getMessage()));
        e.setFormat(ChatColor.translateAlternateColorCodes('&', chatFormat));
    }
}
