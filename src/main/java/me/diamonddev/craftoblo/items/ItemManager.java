package me.diamonddev.craftoblo.items;


import com.coalesce.gui.ItemBuilder;
import lombok.Getter;
import me.diamonddev.craftoblo.Log;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ItemManager {

    @Getter
    List<Item> allItems = new ArrayList<>();

    public ItemManager(File file) throws IOException {
        if (allItems.isEmpty())
            return;
        InputStream is = new FileInputStream(file);
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));
        String line = buf.readLine();
        StringBuilder sb = new StringBuilder();
        while (line != null) {
            sb.append(line).append("\n");
            line = buf.readLine();
        }
        String rawJson = sb.toString();
        JSONObject json = new JSONObject(rawJson);
        if (json.has("armor")) {
            JSONArray armor = json.getJSONArray("armor");
            for (Object obj : armor) {
                Collection<String> keySet = ((JSONObject) obj).keySet();
                List<String> keyList = new ArrayList<>();
                keyList.addAll(keySet);
                JSONArray armorSec = ((JSONObject) obj).getJSONArray(keyList.get(0).toString());
                for (Object item : armorSec) {
                    final JSONObject itemJson = (JSONObject) item;
                    registerItem(buildItem(itemJson));
                }
            }
        }
        Log.info("Total items loaded: " + allItems.size());
        Log.info("Items loaded: ");
        for (Item item : allItems) {
            Log.info("  - " + item.getName());
        }
    }

    private void registerItem(Item item) {
        if (!allItems.contains(item))
            allItems.add(item);
    }

    public Item getItem(String itemId) {
        for (Item item : allItems)
            if (item.getId().equalsIgnoreCase(itemId))
                return item;
        return null;
    }

    public List<Item> getItems(ItemArmorType itemArmorType) {
        List<Item> items = new ArrayList<>();
        return items;
    }

    public List<Item> getItems(ItemWeaponType itemWeaponType) {
        List<Item> items = new ArrayList<>();
        return items;
    }

    public List<Item> getItems(ItemQuality itemQuality) {
        List<Item> items = new ArrayList<>();
        for (Item item : allItems)
            if (item.getQuality().equals(itemQuality))
                items.add(item);
        return items;
    }

    public ItemStack getItemStack(Item item) {
        List<String> lore = new ArrayList<>();
        if (item.isArmor()) {
            lore.add(ChatColor.GRAY + "" + ChatColor.ITALIC + item.getArmorType().name());
            lore.add(" ");
            lore.add(ChatColor.WHITE + "" + ChatColor.BOLD + item.getMinArmor() + "-" + item.getMaxArmor());
            lore.add(ChatColor.GRAY + "" + ChatColor.ITALIC + "Armor");
            lore.add(" ");
        }

        if (lore.size() > 0)
            if (lore.get(lore.size() - 1).equals(" "))
                lore.remove(lore.size() - 1);
        ItemBuilder builder = item.getItemBuilder()
                .displayName(item.getQuality().getColor() + item.getName())
                .lore(lore)
                .itemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES);
        return builder.build();

    }

    public Item buildItem(final JSONObject jsonObject) {
        String id = jsonObject.getString("name").replaceAll(" ", "_").replaceAll("'", "");
        String checkId = id;
        int dupeName = 1;
        while (getItem(checkId) == null) {
            checkId = id + "_" + dupeName;
            dupeName++;
        }
        id = checkId;
        Material material;
        try {
            material = Material.valueOf(jsonObject.getString("material"));
        } catch (Exception e) {
            material = Material.BARRIER;
        }
        int damage = jsonObject.getInt("damage");

        final String _id = id;
        final String _name = jsonObject.getString("name");
        final Material _material = material;
        final int _damage = damage;
        return new Item() {

            @Override
            public String getId() {
                return _id;
            }

            @Override
            public String getName() {
                return _name;
            }

            @Override
            public ItemBuilder getItemBuilder() {
                return null;
            }

            @Override
            public ItemStack getItem() {
                return null;
            }

            @Override
            public ItemQuality getQuality() {
                return null;
            }

            @Override
            public int getRequierdLevel() {
                return 0;
            }

            @Override
            public boolean isArmor() {
                return false;
            }

            @Override
            public double getMaxArmor() {
                return 0;
            }

            @Override
            public double getMinArmor() {
                return 0;
            }

            @Override
            public ItemArmorType getArmorType() {
                return null;
            }

            @Override
            public boolean isWeapon() {
                return false;
            }

            @Override
            public double getMaxDamagePerSecond() {
                return 0;
            }

            @Override
            public double getMinDamagePerSecond() {
                return 0;
            }

            @Override
            public double getAttacksPerSecond() {
                return 0;
            }

            @Override
            public ItemWeaponType getWeaponType() {
                return null;
            }
        };
    }
}
