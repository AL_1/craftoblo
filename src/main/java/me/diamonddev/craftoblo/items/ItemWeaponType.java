package me.diamonddev.craftoblo.items;

import lombok.Getter;
import me.diamonddev.craftoblo.classes.ClassType;

public enum ItemWeaponType {
    DAGGERS(ClassType.ALL), SPEARS(ClassType.ALL), CEREMONIAL_KNIVES(ClassType.WITCH_DOCTOR), FIST_WEAPONS(ClassType.MONK), AXES(ClassType.ALL), MACES(ClassType.ALL), POLEARMS(ClassType.ALL),
    STAVES(ClassType.ALL), SWORDS(ClassType.ALL), DAIBO(ClassType.MONK), FLAILS(ClassType.CRUSADER), MIGHTY_WEAPONS(ClassType.BARBARIAN), BOWS(ClassType.ALL), CROSSBOWS(ClassType.ALL),
    HAND_CROSSBOWS(ClassType.DEMON_HUNTER), WANDS(ClassType.WIZARD);

    @Getter
    private final ClassType classType;

    ItemWeaponType(ClassType classType) {
        this.classType = classType;
    }
}
