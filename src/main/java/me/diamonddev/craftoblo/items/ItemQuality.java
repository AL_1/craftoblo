package me.diamonddev.craftoblo.items;


import lombok.Getter;
import org.bukkit.ChatColor;

public enum ItemQuality {
    // Rarity levels are 1 being the most rare
    LEGENDARY(1, ChatColor.GOLD), RARE(2, ChatColor.YELLOW), SET(3, ChatColor.GREEN), MAGIC(4, ChatColor.BLUE), COMMON(5, ChatColor.WHITE);

    @Getter
    private final ChatColor color;

    ItemQuality(int level, ChatColor color) {
        this.color = color;
    }


}
