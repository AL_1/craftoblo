package me.diamonddev.craftoblo.items;

import com.coalesce.gui.ItemBuilder;
import org.bukkit.inventory.ItemStack;

public interface Item {

    String getId();

    String getName();

    ItemBuilder getItemBuilder();

    default ItemStack getItem() {
        return getItemBuilder().build();
    }

    default ItemQuality getQuality() {
        return ItemQuality.COMMON;
    }

    default int getRequierdLevel() {
        return 1;
    }

    default boolean isArmor() {
        return false;
    }

    default double getMaxArmor() {
        return 0.0;
    }

    default double getMinArmor() {
        return 0.0;
    }

    default ItemArmorType getArmorType() {
        return null;
    }

    default boolean isWeapon() {
        return false;
    }

    default double getMaxDamagePerSecond() {
        return 0.0;
    }

    default double getMinDamagePerSecond() {
        return 0.0;
    }

    default double getAttacksPerSecond() {
        return 0.0;
    }

    default ItemWeaponType getWeaponType() {
        return null;
    }
}
