package me.diamonddev.craftoblo.items;

import lombok.Getter;
import me.diamonddev.craftoblo.classes.ClassType;

public enum ItemArmorType {
    HELMS(ClassType.ALL), SPIRIT_STONES(ClassType.MONK), VOODOO_MASKS(ClassType.WITCH_DOCTOR), WIZARD_HATS(ClassType.WIZARD), PAULDRONS(ClassType.ALL), CHEST_ARMOR(ClassType.ALL),
    CLOAKS(ClassType.DEMON_HUNTER), BRACERS(ClassType.ALL), GLOVES(ClassType.ALL), BELTS(ClassType.ALL), MIGHTY_BELTS(ClassType.BARBARIAN), PANTS(ClassType.ALL), BOOTS(ClassType.ALL),
    AMULETS(ClassType.ALL), RINGS(ClassType.ALL), SHIELDS(ClassType.ALL), CRUSADER_SHIELDS(ClassType.CRUSADER), MOJOS(ClassType.WITCH_DOCTOR), ORBS(ClassType.WIZARD),
    QUIVERS(ClassType.DEMON_HUNTER), ENCHANTRESS_FOCUSES(ClassType.ALL), SCOUNDREL_TOKENS(ClassType.ALL), TEMPLAR_RELICS(ClassType.ALL);

    @Getter
    private final ClassType classType;

    ItemArmorType(ClassType classType) {
        this.classType = classType;
    }
}
