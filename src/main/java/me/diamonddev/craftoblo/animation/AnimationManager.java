package me.diamonddev.craftoblo.animation;

import lombok.Getter;
import lombok.Setter;
import me.diamonddev.craftoblo.Log;
import org.bukkit.entity.ArmorStand;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AnimationManager {

    @Getter
    @Setter
    private HashMap<ArmorStand, List<AnimationPlayer>> animationsPlaying = new HashMap<>();
    @Getter
    private List<Animation> allAnimations = new ArrayList<>();

    public void regeisterAnimation(Animation animation) {
        if (animation == null) {
            new NullPointerException("A null animation cannot be registered!");
            return;
        }
        if (!allAnimations.contains(animation))
            allAnimations.add(animation);

        Log.info("Animation " + animation.getName() + " registered (" + animation.getAllFrames().size() + " frames)");
    }

    public Animation getAnimation(String name) {
        for (Animation animation : allAnimations)
            if (animation.getName().equals(name))
                return animation;
        return null;
    }

    public AnimationPlayer playAnimation(ArmorStand stand, Animation animation, int ammount) {
        AnimationPlayer animationPlayer = new AnimationPlayer(stand, animation, ammount);

        if (!animationsPlaying.containsKey(stand))
            animationsPlaying.put(stand, new ArrayList<>());

        animationsPlaying.get(stand).add(animationPlayer);

        return animationPlayer;
    }

    public void stopArmorStandAnimations(ArmorStand stand) {
        if (animationsPlaying == null || animationsPlaying.get(stand) == null)
            return;
        animationsPlaying.get(stand).forEach(animationPlayer -> animationPlayer.cancel(false));
        animationsPlaying.remove(stand);
    }

    public void stopArmorStandAnimation(ArmorStand stand, Animation animation) {
        if (!animationsPlaying.containsKey(stand))
            return;
        animationsPlaying.get(stand).forEach(animationPlayer -> {
            if (animationPlayer.getAnimation().equals(animation))
                animationPlayer.cancel();
        });
    }

    public void stopAnimation(Animation animation) {
        animationsPlaying.keySet().forEach(stand -> animationsPlaying.get(stand).forEach(animationPlayer -> {
            if (animationPlayer.getAnimation().equals(animation))
                animationPlayer.cancel();
        }));
    }

    public void stopAllAnimations() {
        animationsPlaying.values().forEach(animationPlayers -> animationPlayers.forEach(animationPlayer -> animationPlayer.cancel()));
    }

    public boolean isArmorStandPlayingAnimation(ArmorStand stand, Animation animation) {
        if (!animationsPlaying.containsKey(stand))
            return false;
        for (AnimationPlayer animationPlayer : animationsPlaying.get(stand)) {
            if (animation.equals(animationPlayer.getAnimation()))
                return true;
        }
        return false;
    }

}
