package me.diamonddev.craftoblo.animation;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Animation {
    @Getter
    private final List<Frame> allFrames;
    @Getter
    private final String name;

    public Animation(String name, Frame... allFrames) {
        List<Frame> frameList = new ArrayList<>();
        frameList.addAll(Arrays.asList(allFrames));
        this.allFrames = frameList;
        this.name = name;
    }

    public Animation(String name, List<Frame> allFrames) {
        this.allFrames = allFrames;
        this.name = name;
    }

    public int durration() {
        int time = 0;
        for (Frame frame : allFrames) {
            time = time + frame.length();
        }
        return time;
    }

    public int length() {
        return allFrames.size();
    }

    public Frame getFrame(int index) {
        return allFrames.get(index);
    }

    public Frame getFirstFrame() {
        return allFrames.get(0);
    }

    public Frame getLastFrame() {
        return allFrames.get(allFrames.size() - 1);
    }

}
