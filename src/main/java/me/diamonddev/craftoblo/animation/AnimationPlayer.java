package me.diamonddev.craftoblo.animation;

import lombok.Getter;
import me.diamonddev.craftoblo.Craftoblo;
import org.bukkit.entity.ArmorStand;
import org.bukkit.scheduler.BukkitRunnable;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

public class AnimationPlayer extends BukkitRunnable {

    @Getter
    private final ArmorStand armorStand;
    @Getter
    private final Animation animation;
    @Getter
    private final int amount;
    private final AnimationManager animationManager = Craftoblo.gi().getAnimationManager();
    @Getter
    private int onPlay = 0;
    @Getter
    private int onFrame = 0;

    public AnimationPlayer(@NotNull ArmorStand stand, @NotNull Animation animation, int amount) {
        this.armorStand = stand;
        this.animation = animation;
        this.amount = amount;
        if (animation == null) {
            return;
        }
        this.runTaskTimer(Craftoblo.gi(), 0L, 1L);
    }

    @Override
    public void run() {
        if (onFrame > animation.getAllFrames().size() - 1) {
            onFrame = 0;
            onPlay++;
        }
        if (amount != -1)
            if (onPlay > amount)
                cancel();

        Frame frame = animation.getFrame(onFrame);

        if (frame.getHead() != null)
            armorStand.setHeadPose(frame.getHead());
        if (frame.getBody() != null)
            armorStand.setBodyPose(frame.getBody());
        if (frame.getLeftArm() != null)
            armorStand.setLeftArmPose(frame.getLeftArm());
        if (frame.getRightArm() != null)
            armorStand.setRightArmPose(frame.getRightArm());
        if (frame.getLeftLeg() != null)
            armorStand.setLeftLegPose(frame.getLeftLeg());
        if (frame.getRightLeg() != null)
            armorStand.setRightLegPose(frame.getRightLeg());

        onFrame++;
    }

    @Override
    public synchronized void cancel() {
        cancel(true);
    }

    public synchronized void cancel(boolean removeFromList) {
        super.cancel();
        if (removeFromList) {
            HashMap<ArmorStand, List<AnimationPlayer>> playingAnimations = animationManager.getAnimationsPlaying();
            playingAnimations.get(armorStand).remove(this);
            animationManager.setAnimationsPlaying(playingAnimations);
        }
    }
}
