package me.diamonddev.craftoblo.animation;

import me.diamonddev.craftoblo.Craftoblo;
import me.diamonddev.craftoblo.Log;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.util.EulerAngle;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AnimationProssesor {

    public static void buildAnimations(List<File> files) {
        files.forEach(AnimationProssesor::buildAnimation);
    }

    public static Animation buildAnimation(File file) {
        List<Frame> frames = new ArrayList<>();
        int maxFrames = 32;
        int currentFrame = 0;
        while (true) {
            currentFrame++;
            Frame frame = getFrame(file.getName(), currentFrame);
            if (frame == null)
                break;
            frames.add(frame);
            if (currentFrame >= maxFrames)
                break;
        }

        return new Animation(file.getName().split("\\.")[0], frames);
    }

    public static Animation buildAnimation(String name) {
        List<Frame> frames = new ArrayList<>();
        int maxFrames = 32;
        int currentFrame = 0;
        while (true) {
            //Log.info("Building frame " + currentFrame);
            currentFrame++;
            Frame frame = getFrame(name, currentFrame);
            if (frame == null)
                break;
            frames.add(frame);
            if (currentFrame >= maxFrames)
                break;
        }

        Log.info("Animation built and has " + frames.size() + " frames");
        return new Animation(name, frames);
    }

    private static Frame getFrame(String name, int frame) {
        File file = new File(Craftoblo.gi().getDataFolder(), "/animations/" + name + ".yml");
        //Log.info("Animation path: " + file.getAbsolutePath());
        YamlConfiguration data = YamlConfiguration.loadConfiguration(file);
        if (data.contains("Animation.Frames." + String.valueOf(frame))) {
            final Double DEFAULT = -10000.0;
            String framePath = "Animation.Frames." + String.valueOf(frame) + ".";

            EulerAngle head = null;
            EulerAngle body = null;
            EulerAngle leftArm = null;
            EulerAngle rightArm = null;
            EulerAngle leftLeg = null;
            EulerAngle rightLeg = null;

            // Head
            String wp = framePath + "Head.";
            if (data.contains(wp))
                head = new EulerAngle(data.getDouble(wp + "x", 0), data.getDouble(wp + "y", 0),
                        data.getDouble(wp + "z", 0));

            // Body
            wp = framePath + "Body.";
            if (data.contains(wp))
                body = new EulerAngle(data.getDouble(wp + "x", 0), data.getDouble(wp + "y", 0),
                        data.getDouble(wp + "z", 0));

            // Arms
            wp = framePath + "Arms.left.";
            if (data.contains(wp))
                leftArm = new EulerAngle(data.getDouble(wp + "x", 0), data.getDouble(wp + "y", 0),
                        data.getDouble(wp + "z", 0));

            wp = framePath + "Arms.right.";
            if (data.contains(wp))
                rightArm = new EulerAngle(data.getDouble(wp + "x", 0), data.getDouble(wp + "y", 0),
                        data.getDouble(wp + "z", 0));

            // Legs
            wp = framePath + "Legs.left.";
            if (data.contains(wp))
                leftLeg = new EulerAngle(data.getDouble(wp + "x", 0), data.getDouble(wp + "y", 0),
                        data.getDouble(wp + "z", 0));

            wp = framePath + "Legs.right.";
            if (data.contains(wp))
                rightLeg = new EulerAngle(data.getDouble(wp + "x", 0), data.getDouble(wp + "y", 0),
                        data.getDouble(wp + "z", 0));


            return new Frame(head, body, leftArm, rightArm, leftLeg, rightLeg);
        } else {
            return null;
        }
    }
}
