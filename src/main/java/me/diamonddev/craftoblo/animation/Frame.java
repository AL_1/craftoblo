package me.diamonddev.craftoblo.animation;

import org.bukkit.util.EulerAngle;

public class Frame {

    // Head
    private EulerAngle head = null;

    // Body
    private EulerAngle body = null;

    // Arms
    private EulerAngle leftArm = null;
    private EulerAngle rightArm = null;

    // Legs
    private EulerAngle leftLeg = null;
    private EulerAngle rightLeg = null;

    public Frame(EulerAngle head, EulerAngle body, EulerAngle leftArm, EulerAngle rightArm, EulerAngle leftLeg,
                 EulerAngle rightLeg) {
        this.head = head;
        this.body = body;
        this.leftArm = leftArm;
        this.rightArm = rightArm;
        this.leftLeg = leftLeg;
        this.rightLeg = rightLeg;
    }

    public int length() {
        return 0;
    }

    public EulerAngle getHead() {
        return head;
    }

    public EulerAngle getBody() {
        return body;
    }

    public EulerAngle getLeftArm() {
        return leftArm;
    }

    public EulerAngle getRightArm() {
        return rightArm;
    }

    public EulerAngle getLeftLeg() {
        return leftLeg;
    }

    public EulerAngle getRightLeg() {
        return rightLeg;
    }
}
