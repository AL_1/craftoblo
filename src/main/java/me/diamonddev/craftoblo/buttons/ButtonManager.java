package me.diamonddev.craftoblo.buttons;

import org.bukkit.plugin.Plugin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ButtonManager {

    private static final List<ButtonListener> listeners = new ArrayList<>();

    public static void registerButtonListener(Plugin plugin, ButtonListener listener) {
        if (listener instanceof ButtonListener) {
            listeners.add(listener);
        } else {
            new IOException(plugin.getName() + "tried to register " + listener.getClass().getSimpleName()
                    + " but it is invalid");
        }
    }

    public static List<ButtonListener> getListeners() {
        return listeners;
    }
}
