package me.diamonddev.craftoblo.buttons;

public abstract class ButtonListener {

    public abstract void buttonPressedEvent(ButtonPressEvent e);

}
