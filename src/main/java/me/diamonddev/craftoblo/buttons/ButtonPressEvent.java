package me.diamonddev.craftoblo.buttons;

import org.bukkit.entity.Player;

public class ButtonPressEvent {

    private ButtonType buttonType = null;
    private Player player = null;
    private boolean canceled = false;
    private int itemSlot = 0;

    public ButtonPressEvent(ButtonType button, Player p, int buttonNumber) {
        this.player = p;
        this.buttonType = button;
        this.itemSlot = buttonNumber;
    }

    public Player getPlayer() {
        return player;
    }

    public ButtonType getButtonPressed() {
        return buttonType;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public int getButtonPressedToInt() {
        return itemSlot;
    }
}
