package me.diamonddev.craftoblo.buttons;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class ButtonInputListener implements Listener {

    public void onMove(PlayerMoveEvent e) {
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void handSwitch(PlayerItemHeldEvent e) {
        int to = e.getNewSlot();
        ButtonType button = ButtonType.ONE;
        switch (to) {
            case 0:
                button = ButtonType.ONE;
                break;
            case 1:
                button = ButtonType.TWO;
                break;
            case 2:
                button = ButtonType.THREE;
                break;
            case 3:
                button = ButtonType.FOUR;
                break;
            case 4:
                button = ButtonType.FIVE;
                break;
            case 5:
                button = ButtonType.SIX;
                break;
            case 6:
                button = ButtonType.SEVEN;
                break;
            case 7:
                button = ButtonType.EIGHT;
                break;
            case 8:
                button = ButtonType.NINE;
                break;

            default:
                break;
        }
        for (ButtonListener listener : ButtonManager.getListeners()) {
            try {
                ButtonPressEvent event = new ButtonPressEvent(button, e.getPlayer(), e.getNewSlot());
                listener.buttonPressedEvent(event);
                e.setCancelled(event.isCanceled());
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }
}
