package me.diamonddev.craftoblo.playerdata;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ActionBar {

    private static Class<?> getNmsClass(String nmsClassName) throws ClassNotFoundException {
        return Class.forName("net.minecraft.server."
                + Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + "."
                + nmsClassName);
    }

    private static String getServerVersion() {
        return Bukkit.getServer().getClass().getPackage().getName().substring(23);
    }

    public static void sendAction(Player p, String msg, boolean translateColors) {
        String serverVersion = ActionBar.getServerVersion();
        if (translateColors) {
            msg = ChatColor.translateAlternateColorCodes('&', msg);
        }
        try {
            if (serverVersion.equalsIgnoreCase("v1_9_R1") || serverVersion.equalsIgnoreCase("v1_9_R2")
                    || serverVersion.equalsIgnoreCase("v1_10_R1") || serverVersion.equalsIgnoreCase("v1_11_R1")) {
                Object icbc = ActionBar.getNmsClass("ChatComponentText").getConstructor(String.class)
                        .newInstance(ChatColor.translateAlternateColorCodes('&', msg));
                Object ppoc = ActionBar.getNmsClass("PacketPlayOutChat")
                        .getConstructor(ActionBar.getNmsClass("IChatBaseComponent"), Byte.TYPE)
                        .newInstance(icbc, (byte) 2);
                Object nmsp = p.getClass().getMethod("getHandle", new Class[0]).invoke(p);
                Object pcon = nmsp.getClass().getField("playerConnection").get(nmsp);
                pcon.getClass().getMethod("sendPacket", ActionBar.getNmsClass("Packet")).invoke(pcon, ppoc);
            } else if (serverVersion.equalsIgnoreCase("v1_8_R2") || serverVersion.equalsIgnoreCase("v1_8_R3")) {
                Object icbc = ActionBar.getNmsClass("IChatBaseComponent$ChatSerializer").getMethod("a", String.class)
                        .invoke(null, "{'text': '" + msg + "'}");
                Object ppoc = ActionBar.getNmsClass("PacketPlayOutChat")
                        .getConstructor(ActionBar.getNmsClass("IChatBaseComponent"), Byte.TYPE)
                        .newInstance(icbc, (byte) 2);
                Object nmsp = p.getClass().getMethod("getHandle", new Class[0]).invoke(p);
                Object pcon = nmsp.getClass().getField("playerConnection").get(nmsp);
                pcon.getClass().getMethod("sendPacket", ActionBar.getNmsClass("Packet")).invoke(pcon, ppoc);
            } else {
                Object icbc = ActionBar.getNmsClass("ChatSerializer").getMethod("a", String.class).invoke(null,
                        "{'text': '" + msg + "'}");
                Object ppoc = ActionBar.getNmsClass("PacketPlayOutChat")
                        .getConstructor(ActionBar.getNmsClass("IChatBaseComponent"), Byte.TYPE)
                        .newInstance(icbc, (byte) 2);
                Object nmsp = p.getClass().getMethod("getHandle", new Class[0]).invoke(p);
                Object pcon = nmsp.getClass().getField("playerConnection").get(nmsp);
                pcon.getClass().getMethod("sendPacket", ActionBar.getNmsClass("Packet")).invoke(pcon, ppoc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}