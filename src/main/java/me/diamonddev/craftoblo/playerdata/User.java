package me.diamonddev.craftoblo.playerdata;


import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import me.diamonddev.craftoblo.Craftoblo;
import me.diamonddev.craftoblo.abilitys.*;
import me.diamonddev.craftoblo.classes.ClassType;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class User {

    @Getter
    private final Player player;
    @Getter
    @Setter
    private YamlConfiguration config;
    private final File saveFile;
    @Getter
    @Setter
    private double level = 1;
    @Getter
    @Setter
    private int gold = 0;
    @Getter
    @Setter
    private double orb = 100;
    @Getter
    private ClassType currentClass = ClassType.NONE;
    @Getter
    @Setter
    @NonNull
    private List<AbilityObject> equippedAbilities = new ArrayList<>();

    public User(Player player) throws Exception {

        // Set Player
        this.player = player;

        // Get YAML file
        File userDataDirectory = new File(Craftoblo.gi().getDataFolder(), "/userdata");
        saveFile = new File(userDataDirectory, player.getUniqueId().toString() + ".yml");

        // Create file if needed
        userDataDirectory.mkdirs();
        if (!saveFile.exists())
            saveFile.createNewFile();

        // Set Config
        config = YamlConfiguration.loadConfiguration(saveFile);

        // Set Class
        currentClass = ClassType.valueOf(config.getString("CLASS", "NONE"));

        // Set Gold
        gold = config.getInt(currentClass.toString() + ".GOLD", 0);

        // Set Orb
        orb = config.getDouble(currentClass.toString() + ".ORB", 100);

        // Set Level
        level = config.getDouble(currentClass.toString() + ".LEVEL", 1);

        // Set Equipped Abilities
        List<AbilityObject> equippedAbilities = new ArrayList<>();
        if (currentClass != ClassType.NONE) {
            List<String> equipped = config.getStringList(currentClass.toString() + ".EQUIPPED");
            Collection<AbilityObject> abilities = AbilityManager.getAllAbilitys();
            if (equipped == null || equipped.isEmpty()) {
                Unequiped unequiped = new Unequiped();
                None none = new None();
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(none);
                equippedAbilities.add(none);
                equippedAbilities.add(new HealPotion());
                this.equippedAbilities = equippedAbilities;
            } else {
                for (String s : equipped)
                    for (AbilityObject abilityObject : abilities)
                        if (s.equalsIgnoreCase(abilityObject.getName()))
                            equippedAbilities.add(abilityObject);
                this.equippedAbilities = equippedAbilities;
            }
        } else {
            None none = new None();
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            this.equippedAbilities = equippedAbilities;
        }
    }

    public void sendMessageAsSelf(String s) {
        String prefix = "";
        if (player.isOp())
            prefix = ChatColor.DARK_PURPLE + "*";
        player.sendMessage(prefix + currentClass.getColor() + "You" + ChatColor.DARK_GRAY + "" + ChatColor.BOLD + " » " + ChatColor.GOLD + s);
    }

    public void setCurrentClass(ClassType classType) {
        save();

        // Reset Class
        currentClass = classType;

        // Reset Orb
        setOrb(config.getDouble(currentClass.toString() + ".ORB", 100));

        // Reset Level
        setLevel(config.getDouble(currentClass.toString() + ".LEVEL", 1));

        // Reset Equipped Abilities
        List<AbilityObject> equippedAbilities = new ArrayList<>();
        if (currentClass != ClassType.NONE) {
            List<String> equipped = config.getStringList(currentClass.toString() + ".EQUIPPED");
            Collection<AbilityObject> abilities = AbilityManager.getAllAbilitys();
            if (equipped == null || equipped.isEmpty()) {
                Unequiped unequiped = new Unequiped();
                None none = new None();
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(none);
                equippedAbilities.add(none);
                equippedAbilities.add(new HealPotion());
                this.equippedAbilities = equippedAbilities;
            } else {
                for (String s : equipped)
                    for (AbilityObject abilityObject : abilities)
                        if (s.equalsIgnoreCase(abilityObject.getName()))
                            equippedAbilities.add(abilityObject);
                this.equippedAbilities = equippedAbilities;
            }
        } else {
            None none = new None();
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            this.equippedAbilities = equippedAbilities;
        }
    }

    public HashMap<String, Object> getClassTypeInfo(ClassType classType) {
        // Create HashMap
        HashMap<String, Object> classInfo = new HashMap<>();

        // Get Class
        classInfo.put("CLASS", classType);

        // Get Gold
        classInfo.put("GOLD", gold);

        // Get Orb
        classInfo.put("ORB", config.getDouble(classType.toString() + ".ORB", 100));

        // Get Level
        classInfo.put("LEVEL", config.getDouble(classType.toString() + ".LEVEL", 1));

        // Get Abilities
        List<String> equipped = config.getStringList(classType.toString() + ".EQUIPPED");
        Collection<AbilityObject> abilities = AbilityManager.getClassAbilitys(classType);
        List<AbilityObject> equippedAbilities = new ArrayList<>();
        for (String s : equipped)
            for (AbilityObject abilityObject : abilities)
                if (s.equalsIgnoreCase(abilityObject.getName()))
                    equippedAbilities.add(abilityObject);

        classInfo.put("EQUIPPED", equippedAbilities);

        // Send
        return classInfo;
    }

    public void save() {

        // Save set class
        config.set("CLASS", currentClass.toString());

        // Save set gold
        config.set("GOLD", gold);

        // Save set orb
        config.set(currentClass.toString() + ".ORB", orb);

        // Save set level
        config.set(currentClass.toString() + ".LEVEL", level);

        // Save set Abilitys
        List<String> equipped = new ArrayList<>();
        for (AbilityObject abilityObject : equippedAbilities)
            equipped.add(abilityObject.getName());
        config.set(currentClass.toString() + ".EQUIPPED", equipped);

        try {
            config.save(saveFile);
        } catch (IOException e) {
            player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "(!)" + ChatColor.DARK_RED + " There was an error saving your data, please contact an admin!");
            e.printStackTrace();
        }
    }

    public void reload() {

        // Reset Class
        currentClass = ClassType.valueOf(config.getString("CLASS"));

        // Reset Orb
        setOrb(config.getDouble(currentClass.toString() + ".ORB", 100));

        // Reset Level
        setLevel(config.getDouble(currentClass.toString() + ".LEVEL", 1));

        // Reset Equipped Abilities
        List<AbilityObject> equippedAbilities = new ArrayList<>();
        if (currentClass != ClassType.NONE) {
            List<String> equipped = config.getStringList(currentClass.toString() + ".EQUIPPED");
            Collection<AbilityObject> abilities = AbilityManager.getAllAbilitys();
            if (equipped == null || equipped.isEmpty()) {
                Unequiped unequiped = new Unequiped();
                None none = new None();
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(unequiped);
                equippedAbilities.add(none);
                equippedAbilities.add(none);
                equippedAbilities.add(new HealPotion());
                this.equippedAbilities = equippedAbilities;
            } else {
                for (String s : equipped)
                    for (AbilityObject abilityObject : abilities)
                        if (s.equalsIgnoreCase(abilityObject.getName()))
                            equippedAbilities.add(abilityObject);
                this.equippedAbilities = equippedAbilities;
            }
        } else {
            None none = new None();
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            equippedAbilities.add(none);
            this.equippedAbilities = equippedAbilities;
        }
    }
}
