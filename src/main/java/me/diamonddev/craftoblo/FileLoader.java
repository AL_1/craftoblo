package me.diamonddev.craftoblo;

import org.bukkit.Bukkit;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

class FileLoader {

    private static final Logger logger = Bukkit.getServer().getLogger();
    private static final File dataFolder = Craftoblo.gi().getDataFolder();

    private static final String[] filesToLoad = {};

    public static void CheckFiles() {
        Log.debug(Level.INFO, "Checking Files...");
        if (Craftoblo.gi().getConfig().getBoolean("Options.resetPlugin", true)) {
            Log.debug(Level.INFO, "Copying Files...");
            for (String filePath : filesToLoad) {
                saveResource(filePath, false);
            }
            Craftoblo.gi().getConfig().set("Options.resetPlugin", false);
        }
        Log.debug(Level.INFO, "Done!");
    }

    private static void saveResource(String resourcePath, boolean replace) {
        if (resourcePath == null || resourcePath.equals("")) {
            throw new IllegalArgumentException("ResourcePath cannot be null or empty");
        }

        resourcePath = resourcePath.replace('\\', '/');
        InputStream in = Craftoblo.gi().getResource(resourcePath);
        if (in == null) {
            throw new IllegalArgumentException("The embedded resource '" + resourcePath + "' cannot be found");
        }

        File outFile = new File(dataFolder, resourcePath);
        int lastIndex = resourcePath.lastIndexOf('/');
        File outDir = new File(dataFolder, resourcePath.substring(0, lastIndex >= 0 ? lastIndex : 0));

        if (!outDir.exists()) {
            outDir.mkdirs();
        }

        try {
            if (!outFile.exists() || replace) {
                OutputStream out = new FileOutputStream(outFile);
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                out.close();
                in.close();
            } else {
                /*
				 * logger.log(Level.WARNING, "Could not save " +
				 * outFile.getName() + " to " + outFile + " because " +
				 * outFile.getName() + " already exists.");
				 */
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Could not save " + outFile.getName() + " to " + outFile, ex);
        }
    }
}
