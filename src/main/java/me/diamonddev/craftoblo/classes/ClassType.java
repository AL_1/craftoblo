package me.diamonddev.craftoblo.classes;

import com.coalesce.gui.ItemBuilder;
import lombok.Getter;
import me.diamonddev.craftoblo.Craftoblo;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.function.Consumer;

public enum ClassType {
    BARBARIAN("Barbarian", ChatColor.DARK_RED, ChatColor.RED + "Fury",
            new ItemBuilder(Material.IRON_AXE).displayName(ChatColor.DARK_RED + "Barbarian").itemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS).build(),
            player -> player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_PLACE, 1, 1)),

    CRUSADER("Crusader", ChatColor.BLUE, ChatColor.GRAY + "Wrath",
            new ItemBuilder(Material.SHIELD).displayName(ChatColor.BLUE + "Crusader").itemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS).build(),
            player -> player.playSound(player.getLocation(), Sound.BLOCK_SHULKER_BOX_OPEN, 1, 1)),

    DEMON_HUNTER("Demon Hunter", ChatColor.GRAY, ChatColor.DARK_RED + "Hatred",
            new ItemBuilder(Material.BOW).displayName(ChatColor.GRAY + "Demon Hunter").itemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS).build(),
            player -> new BukkitRunnable() {
                int times = 3;

                public void run() {
                    player.playSound(player.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1, 1);
                    --times;
                    if (times == 0)
                        this.cancel();
                }
            }.runTaskTimer(Craftoblo.gi(), 0, 3)),

    MONK("Monk", ChatColor.GOLD, ChatColor.GOLD + "Spirit",
            new ItemBuilder(Material.STICK).displayName(ChatColor.GOLD + "Monk").itemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS).build(),
            player -> player.playSound(player.getLocation(), Sound.ENTITY_ELDER_GUARDIAN_CURSE, 1, 1)),

    WITCH_DOCTOR("Witch Doctor", ChatColor.DARK_GREEN, ChatColor.BLUE + "Mana",
            new ItemBuilder(Material.SKULL_ITEM).displayName(ChatColor.DARK_GREEN + "Witch Doctor").itemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS).build(),
            player -> player.playSound(player.getLocation(), Sound.ENTITY_WITCH_AMBIENT, 1, 1)),

    WIZARD("Wizard", ChatColor.DARK_PURPLE, ChatColor.DARK_PURPLE + "Arcane Power",
            new ItemBuilder(Material.BLAZE_ROD).displayName(ChatColor.DARK_PURPLE + "Wizard").itemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS).build(),
            player -> player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1)),

    NECROMANCER("Necromancer", ChatColor.YELLOW, ChatColor.DARK_GRAY + "Darkness",
            new ItemBuilder(Material.EYE_OF_ENDER).displayName(ChatColor.YELLOW + "Necromancer").itemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS).build(),
            player -> player.playSound(player.getLocation(), Sound.ENTITY_ENDERDRAGON_GROWL, 1, 1)),

    NONE("None", ChatColor.WHITE, "None",
            new ItemBuilder(Material.BARRIER).displayName(ChatColor.WHITE + "None").itemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS).build(),
            player -> {
            }),

    ALL("None", ChatColor.WHITE, "None",
            new ItemBuilder(Material.BARRIER).displayName(ChatColor.WHITE + "None").itemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS).build(),
            player -> {
            });

    @Getter
    private final String name;
    @Getter
    private final ChatColor color;
    @Getter
    private final String orbName;
    @Getter
    private final ItemStack icon;
    @Getter
    private final Consumer<Player> onSelect;


    ClassType(String name, ChatColor color, String orbName, ItemStack icon, Consumer<Player> onSelect) {
        this.name = name;
        this.color = color;
        this.orbName = orbName;
        this.icon = icon;
        this.onSelect = onSelect;
    }

    public String getFullName(ChatColor... effects) {
        StringBuilder sb = new StringBuilder();
        sb.append(color);
        for (ChatColor effect : effects)
            sb.append(effect);
        sb.append(name);
        return sb.toString();
    }

    public static ClassType[] getRealClasses() {
        return new ClassType[]{BARBARIAN, CRUSADER, DEMON_HUNTER, MONK, WITCH_DOCTOR, WIZARD, NECROMANCER};
    }

    public void onSelect(Player player) {
        this.onSelect.accept(player);
    }
}
