package me.diamonddev.craftoblo;

import me.diamonddev.craftoblo.abilitys.AbilityListener;
import me.diamonddev.craftoblo.abilitys.AbilityManager;
import me.diamonddev.craftoblo.animation.AnimationProssesor;
import me.diamonddev.craftoblo.buttons.ButtonInputListener;
import me.diamonddev.craftoblo.buttons.ButtonManager;
import me.diamonddev.craftoblo.camera.CameraListener;
import me.diamonddev.craftoblo.chat.ChatListener;
import me.diamonddev.craftoblo.commands.*;
import me.diamonddev.craftoblo.items.ItemManager;
import me.diamonddev.craftoblo.menus.SkillsMenu;
import me.diamonddev.craftoblo.playerdata.DataManager;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.plugin.PluginManager;

import java.io.File;
import java.io.IOException;

class Registers {

    static void registerAll() {
        registerAbilitys();
        registerAnimations();
        registerButtonEvents();
        registerCommands();
        registerEvents();
        registerItems();
    }

    private static void registerAnimations() {
        Log.info("Building and registering animation DEFAULT_WALK");
        Craftoblo.gi().getAnimationManager().regeisterAnimation(AnimationProssesor.buildAnimation("DEFAULT_WALK"));
    }

    private static void registerAbilitys() {
        AbilityManager.registerDefaultAbilitys();
    }

    @SuppressWarnings("*")
    private static void registerItems() {
        try {
            Craftoblo.gi().setItemManager(new ItemManager(new File(Craftoblo.gi().getDataFolder(), "items.json")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void registerButtonEvents() {
        ButtonManager.registerButtonListener(Craftoblo.gi(), new AbilityListener());
    }

    private static void registerEvents() {
        PluginManager pmanager = Bukkit.getServer().getPluginManager();
        pmanager.registerEvents(new SkillsMenu(), Craftoblo.gi());
        pmanager.registerEvents(new DataManager(), Craftoblo.gi());
        pmanager.registerEvents(new CameraListener(), Craftoblo.gi());
        pmanager.registerEvents(new ChatListener(), Craftoblo.gi());
        pmanager.registerEvents(new ButtonInputListener(), Craftoblo.gi());
    }

    private static void registerCommands() {
        Server s = Bukkit.getServer();
        s.getPluginCommand("economy").setExecutor(new EcomomyCommand());
        s.getPluginCommand("class").setExecutor(new ClassSelectionCommand());
        s.getPluginCommand("gold").setExecutor(new GoldCommand());
        s.getPluginCommand("camera").setExecutor(new CameraCommand());
        s.getPluginCommand("ranking").setExecutor(new RankingCommand());
        s.getPluginCommand("animate").setExecutor(new AnimationCommand());
        s.getPluginCommand("getItem").setExecutor(new GetItemCommand());
        s.getPluginCommand("level").setExecutor(new LevelCommand());
        s.getPluginCommand("fixme").setExecutor(new FixCommand());
    }
}
