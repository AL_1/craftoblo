package me.diamonddev.craftoblo.camera;

import lombok.Getter;
import me.diamonddev.craftoblo.Craftoblo;
import me.diamonddev.craftoblo.playerdata.DataManager;
import me.diamonddev.craftoblo.playerdata.User;
import me.diamonddev.craftoblo.scoreboard.SetupScorboard;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.UUID;

public class CameraManager {
    private static CameraManager mainManager = null;
    @Getter
    private final HashMap<UUID, ArmorStand> atachers = new HashMap<>();
    private final HashMap<UUID, GameMode> orgGamemode = new HashMap<>();
    private final HashMap<UUID, Float[]> cachedAngles = new HashMap<>();

    public static CameraManager getManager() {
        if (mainManager == null)
            mainManager = new CameraManager();
        return mainManager;
    }

    public void attach(Player p, double xOffest, double yOffest, double zOffest) {
        User user = DataManager.getUser(p);
        if (isAttatched(p)) {
            atachers.get(p.getUniqueId()).setHealth(0);
            atachers.remove(p.getUniqueId());
        }
        ArmorStand stand = (ArmorStand) p.getWorld().spawnEntity(p.getLocation().subtract(0, 0, 0),
                EntityType.ARMOR_STAND);
        Spider enemy = (Spider) p.getWorld().spawnEntity(p.getLocation(), EntityType.SPIDER);
        Pig hat = (Pig) p.getWorld().spawnEntity(p.getLocation(), EntityType.PIG);
        enemy.remove();
        orgGamemode.put(p.getUniqueId(), p.getGameMode());
        atachers.put(p.getUniqueId(), stand);

        Location ploc = stand.getLocation().add(xOffest, yOffest, zOffest);
        p.teleport(ploc);
        Float[] rotations = faceLocation(p, stand.getEyeLocation());
        cachedAngles.put(p.getUniqueId(), rotations);
        ploc.setYaw(rotations[0]);
        ploc.setPitch(rotations[1]);

        p.teleport(ploc);
        p.setGameMode(GameMode.SURVIVAL);
        p.setAllowFlight(true);
        p.setFlying(true);
        p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 9999 * 20, 1, true, false));
        p.setInvulnerable(true);

        stand.setBasePlate(false);
        stand.setArms(true);
        stand.setAI(false);
        stand.setCollidable(false);
        stand.setInvulnerable(true);
        stand.setGravity(false);

        hat.setAI(false);
        // hat.setInvulnerable(true);
        hat.setGravity(false);
        hat.setCustomName("HEART_" + p.getUniqueId().toString());
        hat.setCustomNameVisible(false);
        hat.teleport(ploc.clone().add(0, 1, 0));
        hat.setSilent(true);

        hat.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 9999 * 20, 1, true, false));
        hat.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 9999 * 20, 20, true, false));

        enemy.setTarget(hat);
        String rank = "";
        if (p.isOp())
            rank = ChatColor.DARK_PURPLE + "*";
        try {
            stand.setCustomName(rank + ChatColor.RESET
                    + user.getCurrentClass().getColor() + p.getDisplayName() + " ("
                    + user.getCurrentClass().getName() + ")");
        } catch (Exception e) {
            stand.setCustomName(ChatColor.RED + p.getDisplayName() + " (ERROR)");
            p.sendMessage(ChatColor.RED
                    + "There was an error when getting your data, please contact an administrator about this problem.");
        }
        p.setPlayerListName(rank + ChatColor.RESET
                + user.getCurrentClass().getColor()
                + p.getDisplayName());
        stand.setCustomNameVisible(true);
        ItemStack skull = new ItemStack(Material.SKULL_ITEM);
        skull.setDurability((short) 3);
        SkullMeta sm = (SkullMeta) skull.getItemMeta();
        sm.setOwner(p.getName());
        skull.setItemMeta(sm);
        stand.setHelmet(skull);
    }

    public Float[] getCachedAngles(Player player) {
        return cachedAngles.get(player.getUniqueId());
    }

    public Float[] faceLocation(Entity toFace, Location loc) {
        Location entityLoc = toFace.getLocation();
        if (toFace instanceof LivingEntity) {
            entityLoc = ((LivingEntity) toFace).getEyeLocation();
        }
        double diffX = loc.getX() - entityLoc.getX();
        double diffY = loc.getY() - entityLoc.getY();
        double diffZ = loc.getZ() - entityLoc.getZ();
        double dist = Math.sqrt(diffX * diffX + diffZ * diffZ);
        float yaw = (float) (Math.atan2(diffZ, diffX) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float) -(Math.atan2(diffY, dist) * 180.0D / Math.PI);
        return new Float[]{entityLoc.getYaw() + wrapAngleTo180(yaw - entityLoc.getYaw()), entityLoc.getPitch() + wrapAngleTo180(pitch - entityLoc.getPitch())};
    }

    public float wrapAngleTo180(float value) {
        value = value % 360.0F;
        if (value >= 180.0F)
            value -= 360.0F;
        if (value < -180.0F)
            value += 360.0F;
        return value;
    }

    public void detach(Player p) {
        if (atachers == null)
            return;
        if (!atachers.containsKey(p.getUniqueId()))
            return;
        ArmorStand stand = atachers.get(p.getUniqueId());
        for (Entity entity : stand.getNearbyEntities(5, 5, 5))
            if (entity.getName().equals("HEART_" + p.getUniqueId().toString()))
                entity.remove();
        stand.remove();
        Craftoblo.gi().getAnimationManager().stopArmorStandAnimations(stand);
        SetupScorboard.bossBars.get(p.getUniqueId()).setVisible(false);
        SetupScorboard.bossBars.get(p.getUniqueId()).removeAll();

        atachers.remove(p.getUniqueId());
        p.setAllowFlight(false);
        p.setInvulnerable(false);
        p.setFlying(false);
        p.setGameMode(orgGamemode.get(p.getUniqueId()));
        orgGamemode.remove(p.getUniqueId());
        p.teleport(stand);
        p.removePotionEffect(PotionEffectType.INVISIBILITY);

    }

    public void moveArmorStand(Player p, Location loc) {
        if (atachers == null)
            return;
        if (!atachers.containsKey(p.getUniqueId())) {
        }
        // ArmorStand stand = atachers.get(p.getUniqueId());

    }

    public boolean isAttatched(Player p) {
        return atachers != null && atachers.containsKey(p.getUniqueId());
    }

    public ArmorStand getArmorStand(Player p) {
        return atachers.get(p.getUniqueId());
    }


}