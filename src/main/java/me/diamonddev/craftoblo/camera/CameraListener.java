package me.diamonddev.craftoblo.camera;

import me.diamonddev.craftoblo.Craftoblo;
import me.diamonddev.craftoblo.Variables;
import me.diamonddev.craftoblo.animation.Animation;
import me.diamonddev.craftoblo.menus.SkillsMenu;
import me.diamonddev.craftoblo.playerdata.ActionBar;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class CameraListener implements Listener {
    private final boolean debug = Craftoblo.gi().getConfig().getBoolean("Options.Debug");
    private final Material[] allowedBlocks = {Material.AIR, Material.TORCH, Material.RAILS, Material.ACTIVATOR_RAIL,
            Material.DETECTOR_RAIL, Material.RAILS, Material.BROWN_MUSHROOM, Material.RED_MUSHROOM, Material.LONG_GRASS,
            Material.SAPLING, Material.YELLOW_FLOWER, Material.CHORUS_FLOWER, Material.FLOWER_POT, Material.SNOW,
            Material.LADDER, Material.VINE, Material.DOUBLE_PLANT, Material.END_ROD, Material.SKULL, Material.BANNER,
            Material.STANDING_BANNER, Material.WALL_BANNER, Material.SIGN, Material.CARPET, Material.WHEAT,
            Material.STONE_BUTTON, Material.WOOD_BUTTON, Material.CARROT, Material.POTATO, Material.BEETROOT_BLOCK,
            Material.SUGAR_CANE_BLOCK, Material.DEAD_BUSH, Material.WEB, Material.REDSTONE_WIRE, Material.LEVER,
            Material.STONE_PLATE, Material.WOOD_PLATE, Material.GOLD_PLATE, Material.IRON_PLATE, Material.WATER,
            Material.STATIONARY_WATER};
    private final List<Material> allowedBlocksList = Arrays.asList(allowedBlocks);
    private final Animation walkAnimation;

    public CameraListener() {
        walkAnimation = Craftoblo.gi().getAnimationManager().getAnimation("DEFAULT_WALK");
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onMove(PlayerMoveEvent e) {
        Location from = e.getFrom().clone();
        Location to = e.getTo().clone();

        Player p = e.getPlayer();
        if (!CameraManager.getManager().isAttatched(p))
            return;
        e.setCancelled(false);
        ArmorStand stand = CameraManager.getManager().getArmorStand(p);
        String moveDirection = CameraMath.getMovingDirection(e.getFrom().clone(), e.getTo().clone()).toUpperCase();
        float asYaw = 1;
        switch (moveDirection) {
            case "NORTH":
                asYaw = 180;
                break;
            case "NORTHEAST":
                asYaw = 225;
                break;
            case "EAST":
                asYaw = 270;
                break;
            case "SOUTHEAST":
                asYaw = 315;
                break;
            case "SOUTH":
                asYaw = 0;
                break;
            case "SOUTHWEST":
                asYaw = 45;
                break;
            case "WEST":
                asYaw = 90;
                break;
            case "NORTHWEST":
                asYaw = 135;
                break;
            default:
                asYaw = e.getFrom().getYaw();
                break;
        }
        stand.getLocation().setYaw(asYaw);

        // Player
        Location ploc = e.getTo().clone();
        Location sloc = p.getLocation().clone();
        Float[] rotations = CameraManager.getManager().getCachedAngles(p);
        ploc.setYaw(rotations[0]);
        ploc.setPitch(rotations[1]);
        sloc.setYaw(asYaw);
        sloc.setPitch(0);

        e.setTo(ploc.add(0, 0, 0));
        Material toType = e.getTo().clone().subtract(5, 4.9, 5).getBlock().getType();
        if (!toType.equals(Material.AIR)) {
            if (!allowedBlocksList.contains(toType)) {
                e.setCancelled(true);
                return;
            }
        }
        sloc.subtract(5, 5, 5);
        stand.teleport(sloc);
        stand.teleport(sloc);

        for (Entity entity : stand.getNearbyEntities(5, 5, 5))
            if (entity.getName().equals("HEART_" + p.getUniqueId().toString()) && entity.getType() == EntityType.PIG)
                entity.teleport(sloc.clone().add(0, 1, 0));

        Variables.moving.put(p.getUniqueId(), Variables.movingTimeout);
        if (!Craftoblo.gi().getAnimationManager().isArmorStandPlayingAnimation(stand, walkAnimation))
            Craftoblo.gi().getAnimationManager().playAnimation(stand, walkAnimation, -1);

        if (debug) {
            double FX = from.getX();
            double TX = to.getX();
            double DX = FX - TX;
            double FZ = from.getZ();
            double TZ = to.getZ();
            double DZ = FZ - TZ;
            ActionBar.sendAction(p,
                    moveDirection + " (Set To: " + asYaw + ") (Is Set: " + stand.getLocation().getYaw() + ")", false);
            p.sendMessage("X = " + DX + "\nZ = " + DZ);
        }

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onLeave(PlayerQuitEvent e) {
        if (CameraManager.getManager().isAttatched(e.getPlayer()))
            CameraManager.getManager().detach(e.getPlayer());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onJoin(final PlayerJoinEvent e) {
        Location spawnLoc = e.getPlayer().getWorld().getSpawnLocation().add(0.5, 0, 0.5);
        e.getPlayer().teleport(spawnLoc);
        new BukkitRunnable() {

            public void run() {
                CameraManager.getManager().attach(e.getPlayer(), 5, 5, 5);

            }
        }.runTaskLater(Craftoblo.gi(), 30);
    }

    @EventHandler
    public void creativeInvClick(InventoryClickEvent e) {
        if (e.getClickedInventory() == null)
            return;
        Player p = (Player) e.getWhoClicked();
        if (e.getClickedInventory().getType().equals(InventoryType.PLAYER) && CameraManager.getManager().isAttatched(p)) {
            e.setCancelled(true);

        }
    }

    @EventHandler
    public void runeClick(InventoryClickEvent e) {
        if (e.getClickedInventory() == null)
            return;
        Player p = (Player) e.getWhoClicked();
        if (e.getClickedInventory().getType().equals(InventoryType.PLAYER) && CameraManager.getManager().isAttatched(p))
            if (e.getSlot() >= 9 && e.getSlot() <= 35)
                p.sendMessage(ChatColor.AQUA + "The runes menu has not yet been created. (" + e.getSlot() + ")");

    }

    @EventHandler
    public void invClock(InventoryClickEvent e) {
        if (e.getClickedInventory() == null)
            return;
        Player p = (Player) e.getWhoClicked();
        if (CameraManager.getManager().isAttatched(p))
            if (e.getClickedInventory().getType().equals(InventoryType.PLAYER)) {
                if (e.getSlot() >= 36 && e.getSlot() <= 39)
                    p.sendMessage(ChatColor.AQUA + "The inventory menu has not yet been created. (" + e.getSlot() + ")");
            } else if (e.getClickedInventory().getType().equals(InventoryType.CRAFTING)) {
                p.sendMessage(ChatColor.AQUA + "The inventory menu has not yet been created. (" + e.getSlot() + ")");
            }

    }

    @EventHandler
    public void abilityClick(InventoryClickEvent e) {
        if (e.getClickedInventory() == null)
            return;
        Player p = (Player) e.getWhoClicked();
        if (e.getClickedInventory().getType().equals(InventoryType.PLAYER) && CameraManager.getManager().isAttatched(p))
            if (e.getSlot() >= 0 && e.getSlot() <= 5)
                new SkillsMenu().open(p);

    }

    @EventHandler
    public void playerDrop(PlayerDropItemEvent e) {
        if (CameraManager.getManager().isAttatched(e.getPlayer())) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onInvOpen(InventoryOpenEvent e) {
        if (CameraManager.getManager().isAttatched((Player) e.getPlayer())) {
            if (e.getInventory() != null && e.getInventory().getType() == InventoryType.PLAYER) {
                e.setCancelled(true);
            }
        }

    }

    @EventHandler
    public void onEnemyHitHeart(EntityDamageByEntityEvent e) {
        if (e.getEntity().getType() != EntityType.PIG)
            return;
        if (e.getEntity().getCustomName() == null)
            return;
        if (!e.getEntity().getCustomName().startsWith("HEART_"))
            return;
        Player p = Bukkit.getPlayer(UUID.fromString(e.getEntity().getCustomName().replaceFirst("HEART_", "")));
        // p.sendMessage("You were hit by a " +
        // e.getDamager().getType().toString());
        p.setInvulnerable(false);
        p.damage(1);
        p.setHealth(p.getHealth() + 1);
        p.setInvulnerable(true);

    }

    @EventHandler
    public void onEnemyTarget(EntityTargetLivingEntityEvent e) {
        if (e.getTarget() instanceof Player) {
            Player p = (Player) e.getTarget();
            for (Entity entity : CameraManager.getManager().getArmorStand(p).getNearbyEntities(5, 5, 5))
                if (entity.getName().equals("HEART_" + p.getUniqueId().toString())
                        && entity.getType() == EntityType.PIG)
                    e.setTarget(entity);
        }
    }

}
