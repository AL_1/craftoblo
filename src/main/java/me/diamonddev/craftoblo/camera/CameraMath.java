package me.diamonddev.craftoblo.camera;

import org.bukkit.Location;

class CameraMath {
    public static String getMovingDirection(Location from, Location to) {
        // X
        double FX = from.getX();
        double TX = to.getX();
        double DX = FX - TX;
        String X = "";
        if (DX != 0) {
            if (!isPositive(DX)) {
                X = "EAST";
            } else {
                X = "WEST";
            }
        }

        // Z
        double FZ = from.getZ();
        double TZ = to.getZ();
        double DZ = FZ - TZ;
        String Z = "";
        if (DZ != 0) {
            if (isPositive(DZ)) {
                Z = "NORTH";
            } else {
                Z = "SOUTH";
            }
        }

        // Return
        return Z + X;
    }

    private static boolean isPositive(double num) {
        return num >= 0;
    }
}
