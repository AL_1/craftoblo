package me.diamonddev.craftoblo;

import java.io.File;

class MainConfigManager {
    private static void loadConfig() {
        Craftoblo.gi().getConfig().getDefaults();
        Craftoblo.gi().saveDefaultConfig();
        Craftoblo.gi().reloadConfig();
    }

    public static void checkConfig() {
        if (Craftoblo.gi().getConfig().getInt("Config") == 4) {
            loadConfig();
        } else {
            createNewConfig();
        }
    }

    private static void createNewConfig() {
        File oldFile = new File(Craftoblo.gi().getDataFolder().getAbsolutePath() + "/config.yml");
        File renamedFile;
        renamedFile = new File(
                Craftoblo.gi().getDataFolder().getAbsolutePath() + "/config_old." + System.currentTimeMillis() + ".yml");
        oldFile.renameTo(renamedFile);
        loadConfig();
    }
}
