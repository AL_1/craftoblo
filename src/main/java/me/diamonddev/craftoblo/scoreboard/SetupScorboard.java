package me.diamonddev.craftoblo.scoreboard;

import me.diamonddev.craftoblo.playerdata.DataManager;
import me.diamonddev.craftoblo.playerdata.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class SetupScorboard {

    public final static HashMap<UUID, BossBar> bossBars = new HashMap<>();

    // @SuppressWarnings("deprecation")
    public static void set(Player p) {
        if (!p.isOnline())
            return;
        User user = DataManager.getUser(p);

        int longest = longest(String.valueOf(user.getGold()).length(), user.getCurrentClass().getName().length(), 10, p.getName().length() + 1,
                user.getCurrentClass().getOrbName().length());
        // SimpleScoreboard ss = new SimpleScoreboard(ChatColor.AQUA +
        // p.getName() + ":");
        SimpleScoreboard ss = new SimpleScoreboard(ending(longest));
        // ss.add(ending(longest), 15);
        ss.add(ChatColor.GREEN + "Gold:", 11);
        ss.add(ChatColor.GOLD + "" + user.getGold(), 10);
        ss.add(" " + ChatColor.RESET, 9);
        ss.add(ChatColor.GREEN + "Class:", 8);
        ss.add(user.getCurrentClass().getFullName(), 7);
        ss.add(" " + ChatColor.RESET + ChatColor.RESET, 6);
        ss.add(ChatColor.GREEN + user.getCurrentClass().getOrbName() + ":", 5);
        ss.add(persentRecolor((long) user.getOrb()), 4);
        ss.add(" " + ChatColor.RESET + ChatColor.RESET + ChatColor.RESET, 3);
        ss.add(ChatColor.GREEN + "Health:", 2);
        ss.add(persentRecolor(Math.round(((p.getHealth() / 20 * p.getHealthScale()) * 1000)) / 200) + ChatColor.RESET, 1);
        ss.add(ending(longest), 0);
        ss.build();
        ss.send(p);
        double fullLevel = user.getLevel();
        String[] levelParts = ("" + fullLevel).split("\\.");
        int levelInt = Integer.parseInt(levelParts[0]);
        double levelPrg = Double.parseDouble("0." + levelParts[1]);
        if (!bossBars.containsKey(p.getUniqueId()))
            bossBars.put(p.getUniqueId(), Bukkit.createBossBar(user.getCurrentClass().getColor() + "" + ChatColor.BOLD + "Level: " + levelInt, BarColor.YELLOW, BarStyle.SOLID));
        BossBar bossBar = bossBars.get(p.getUniqueId());
        if (!bossBar.getPlayers().contains(p))
            bossBar.addPlayer(p);
        bossBar.setTitle(user.getCurrentClass().getColor() + "" + ChatColor.BOLD + "Level: " + levelInt);
        bossBar.setProgress(levelPrg);
        //Bukkit.create
        if (user.getOrb() < 0)
            user.setOrb(0);
        if (user.getOrb() > 100)
            user.setOrb(100);
        p.setExp((float) user.getOrb() / 100);

        //p.setLevel(levelInt);
    }

    private static String ending(int length) {
        StringBuilder str = new StringBuilder(ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH);
        while (length != 0) {
            str.append("-");
            --length;
        }
        return str.toString();
    }

    private static int longest(int... strs) {
        int longest = 0;
        for (int str : strs) {
            if (str > longest)
                longest = str;
        }
        return longest;
    }

    private static String persentRecolor(long pres) {
        // DARK_GREEN = 80 - 100
        // GREEN = 60 - 79
        // YELLOW = 40 - 59
        // GOLD = 20 - 39
        // RED = 0 - 19
        String color = "" + ChatColor.RESET;
        if (pres > 100) {
            color = "" + ChatColor.AQUA;
        } else if (pres >= 80 && pres <= 100) {
            color = "" + ChatColor.DARK_GREEN;
        } else if (pres >= 60 && pres <= 79) {
            color = "" + ChatColor.GREEN;
        } else if (pres >= 40 && pres <= 59) {
            color = "" + ChatColor.YELLOW;
        } else if (pres >= 20 && pres <= 39) {
            color = "" + ChatColor.GOLD;
        } else if (pres >= 0 && pres <= 19) {
            color = "" + ChatColor.RED;
        } else if (pres < 0) {
            color = "" + ChatColor.LIGHT_PURPLE;
        }

        return color + pres + "%";
    }

}
