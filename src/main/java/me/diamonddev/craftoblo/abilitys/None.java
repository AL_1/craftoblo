package me.diamonddev.craftoblo.abilitys;

import me.diamonddev.craftoblo.classes.ClassType;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class None extends AbilityObject {

    @Override
    public void play(int button, Player p) {
    }

    @Override
    public void onCooldown(int button, Player p) {
    }

    @Override
    public String getName() {
        return "NONE";
    }

    @Override
    public ItemStack getItem(Player p) {
        return new ItemStack(Material.AIR);
    }

    @Override
    public int getUnlockLevel(Player p) {
        return 0;
    }

    @Override
    public ClassType getAllowedClass() {
        return ClassType.ALL;
    }
}
