package me.diamonddev.craftoblo.abilitys;

import me.diamonddev.craftoblo.buttons.ButtonListener;
import me.diamonddev.craftoblo.buttons.ButtonPressEvent;
import me.diamonddev.craftoblo.buttons.ButtonType;
import me.diamonddev.craftoblo.camera.CameraManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class AbilityListener extends ButtonListener {

    @Override
    public void buttonPressedEvent(final ButtonPressEvent e) {
        if (!CameraManager.getManager().isAttatched(e.getPlayer()))
            return;
        if (e.getButtonPressed() == ButtonType.EIGHT)
            return;
        Player p = e.getPlayer();
        PlayerInventory inv = p.getInventory();
        inv.setHeldItemSlot(7);
        if (inv.getItem(e.getButtonPressedToInt()) == null) {
            if (e.getButtonPressed() == ButtonType.SEVEN)
                return;
            Unequiped uneq = new Unequiped();
            p.getInventory().setItem(e.getButtonPressedToInt(), uneq.getItem(p));
            uneq.play(e.getButtonPressedToInt(), p);
            return;
        }
        if (inv.getItem(e.getButtonPressedToInt()).getType().equals(Material.AIR)) {
            if (e.getButtonPressed() == ButtonType.SEVEN)
                return;
            Unequiped uneq = new Unequiped();
            p.getInventory().setItem(e.getButtonPressedToInt(), uneq.getItem(p));
            uneq.play(e.getButtonPressedToInt(), p);
            return;
        }
        AbilityObject ability = null;

        for (AbilityObject ab : AbilityManager.getAllAbilitys()) {
            ItemStack item = inv.getItem(e.getButtonPressedToInt()).clone();
            item.setAmount(1);
            ItemStack abItem = ab.getItem(p).clone();
            abItem.setAmount(1);
            if (item.equals(abItem)) {
                ability = ab;
            }
        }
        if (ability == null) {
            if (e.getButtonPressed() == ButtonType.SEVEN)
                return;
            p.getInventory().setItem(e.getButtonPressedToInt(), new Unequiped().getItem(p));
            return;
        }

        if (p.isOp() && p.isSneaking()) {
            ability.play(e.getButtonPressedToInt(), p);
            return;
        }

        if (inv.getItem(e.getButtonPressedToInt()).getAmount() != 1) {
            ability.onCooldown(e.getButtonPressedToInt(), p);
        } else {
            ability.play(e.getButtonPressedToInt(), p);
        }

		/*
         * if (inv.getItem(e.getButtonPressedToInt()).getType() ==
		 * Material.WOOL) { if
		 * (inv.getItem(e.getButtonPressedToInt()).getDurability() == 7) { try {
		 * AbilityCooldown.cooldown(p, e.getButtonPressedToInt(), 10); } catch
		 * (Exception e1) { e1.printStackTrace(); } } }
		 */

    }

}
