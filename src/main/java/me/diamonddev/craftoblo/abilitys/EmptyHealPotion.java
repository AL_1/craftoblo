package me.diamonddev.craftoblo.abilitys;

import com.coalesce.gui.ItemBuilder;
import me.diamonddev.craftoblo.classes.ClassType;
import me.diamonddev.craftoblo.playerdata.DataManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class EmptyHealPotion extends AbilityObject {

    @Override
    public void play(int button, Player p) {
        DataManager.getUser(p).sendMessageAsSelf("I'm all out.");
    }

    @Override
    public String getName() {
        return "EMPTY_HEALTH_POTION";
    }

    @Override
    public void onCooldown(int button, Player p) {
    }

    @Override
    public ItemStack getItem(Player p) {
        return new ItemBuilder(Material.GLASS_BOTTLE)
                .displayName(ChatColor.RED + "Empty Health Potion")
                .itemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS)
                .amount(1)
                .build();
    }

    @Override
    public int getUnlockLevel(Player p) {
        return 0;
    }

    @Override
    public ClassType getAllowedClass() {
        return ClassType.ALL;
    }

}
