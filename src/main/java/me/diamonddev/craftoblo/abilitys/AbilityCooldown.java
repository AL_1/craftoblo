package me.diamonddev.craftoblo.abilitys;

import me.diamonddev.craftoblo.Craftoblo;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

public class AbilityCooldown {
    public static void cooldown(final Player p, final int slot, int cooldown) {
        PlayerInventory inv = p.getInventory();
        if (cooldown < 1)
            cooldown = 0;
        inv.getItem(slot).setAmount(cooldown + 1);
        new BukkitRunnable() {
            final PlayerInventory inv = p.getInventory();

            public void run() {
                ItemStack currentItem = inv.getItem(slot);
                if (currentItem == null || currentItem.getType().equals(Material.AIR) || currentItem.getAmount() == 1
                        || !p.isOnline()) {
                    this.cancel();
                    return;
                }
                currentItem.setAmount(currentItem.getAmount() - 1);
            }
        }.runTaskTimer(Craftoblo.gi(), 0L, 20L);
    }
}
