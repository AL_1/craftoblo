package me.diamonddev.craftoblo.abilitys;

import me.diamonddev.craftoblo.classes.ClassType;
import me.diamonddev.craftoblo.playerdata.ActionBar;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Locked extends AbilityObject {

    @Override
    public void play(int button, Player p) {
        int level = 0;
        switch (button + 1) {
            case 1:
                level = 0;
                break;
            case 2:
                level = 2;
                break;
            case 3:
                level = 4;
                break;
            case 4:
                level = 9;
                break;
            case 5:
                level = 14;
                break;
            case 6:
                level = 19;
                break;
            default:
                break;
        }

        ActionBar.sendAction(p, ChatColor.RED + "This skill will be unlocked at level " + level, false);
    }

    @Override
    public void onCooldown(int button, Player p) {
    }

    @Override
    public String getName() {
        return "LOCKED";
    }

    @Override
    public ItemStack getItem(Player p) {
        ItemStack abilityItem = new ItemStack(Material.WOOL, 1, (short) 7);
        ItemMeta im = abilityItem.getItemMeta();
        im.setDisplayName(ChatColor.DARK_GRAY + "Ability Locked");
        im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS);
        abilityItem.setItemMeta(im);
        return abilityItem;
    }

    @Override
    public int getUnlockLevel(Player p) {
        return 0;
    }

    @Override
    public ClassType getAllowedClass() {
        return ClassType.ALL;
    }

}
