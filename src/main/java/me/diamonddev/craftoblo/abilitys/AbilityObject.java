package me.diamonddev.craftoblo.abilitys;

import me.diamonddev.craftoblo.classes.ClassType;
import me.diamonddev.craftoblo.playerdata.DataManager;
import me.diamonddev.craftoblo.playerdata.User;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class AbilityObject {

    abstract public void play(int button, Player p);

    abstract public ItemStack getItem(Player p);

    abstract public String getName();

    abstract public void onCooldown(int button, Player p);

    abstract public int getUnlockLevel(Player p);

    abstract public ClassType getAllowedClass();

    public ConfigurationSection getItemConfig(Player p) {
        User user = DataManager.getUser(p);
        String section = user.getCurrentClass().name() + ".ITEMDATA." + getName();
        if (!user.getConfig().contains(section))
            user.getConfig().createSection(section);
        return user.getConfig().getConfigurationSection(section);
    }
}
