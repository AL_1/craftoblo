package me.diamonddev.craftoblo.abilitys;

import lombok.Getter;
import lombok.NonNull;
import me.diamonddev.craftoblo.Craftoblo;
import me.diamonddev.craftoblo.classes.ClassType;
import me.diamonddev.craftoblo.playerdata.DataManager;
import me.diamonddev.craftoblo.playerdata.User;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class AbilityManager {
    @Getter
    @NonNull
    private static final List<AbilityObject> allAbilitys = new ArrayList<>();
    @Getter
    private static final HashMap<ClassType, Collection<AbilityObject>> abilitysByClass = new HashMap<>();

    private static void registerAbility(org.bukkit.plugin.Plugin plugin, AbilityObject ability) {
        if (ability != null) {
            ClassType classType = ability.getAllowedClass();

            if (!abilitysByClass.containsKey(classType))
                abilitysByClass.put(ability.getAllowedClass(), new ArrayList<>());

            Collection<AbilityObject> abilityObjects = abilitysByClass.get(classType);
            abilityObjects.add(ability);

            abilitysByClass.put(classType, abilityObjects);

            allAbilitys.add(ability);
        } else
            new IOException(plugin.getName() + " tried to register a class, but it is an invalid ability class!").printStackTrace();

    }

    public static void setPlayersAbilitys(Player p) {
        PlayerInventory inv = p.getInventory();
        inv.clear();
        User user = DataManager.getUser(p);
        inv.setHeldItemSlot(7);
        List<AbilityObject> abilityObject = user.getEquippedAbilities();
        for (int i = 0; i < abilityObject.size(); i++)
            user.getPlayer().getInventory().setItem(i, abilityObject.get(i).getItem(user.getPlayer()));
    }

    public static Collection<AbilityObject> getClassAbilitys(ClassType classType) {
        return abilitysByClass.get(classType);
    }

    public static void registerDefaultAbilitys() {
        registerAbility(Craftoblo.gi(), new Locked());
        registerAbility(Craftoblo.gi(), new HealPotion());
        registerAbility(Craftoblo.gi(), new EmptyHealPotion());
        registerAbility(Craftoblo.gi(), new NearExplode());
        registerAbility(Craftoblo.gi(), new Unequiped());
        registerAbility(Craftoblo.gi(), new None());
    }

}
