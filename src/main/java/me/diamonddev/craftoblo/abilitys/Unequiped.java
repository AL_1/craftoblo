package me.diamonddev.craftoblo.abilitys;

import me.diamonddev.craftoblo.classes.ClassType;
import me.diamonddev.craftoblo.playerdata.ActionBar;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Unequiped extends AbilityObject {

    @Override
    public void play(int button, Player p) {
        ActionBar.sendAction(p, ChatColor.RED + "There is no skill equiped here!", false);
    }

    @Override
    public void onCooldown(int button, Player p) {
    }

    @Override
    public String getName() {
        return "UNEQUIPED";
    }

    @Override
    public ItemStack getItem(Player p) {
        ItemStack abilityItem = new ItemStack(Material.WOOL, 1, (short) 8);
        ItemMeta im = abilityItem.getItemMeta();
        im.setDisplayName(ChatColor.GRAY + "Unequiped");
        im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS);
        abilityItem.setItemMeta(im);
        return abilityItem;
    }

    @Override
    public int getUnlockLevel(Player p) {
        return 0;
    }

    @Override
    public ClassType getAllowedClass() {
        return ClassType.ALL;
    }
}
