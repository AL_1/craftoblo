package me.diamonddev.craftoblo.abilitys;

import com.coalesce.gui.ItemBuilder;
import me.diamonddev.craftoblo.classes.ClassType;
import me.diamonddev.craftoblo.playerdata.DataManager;
import me.diamonddev.craftoblo.playerdata.User;
import me.diamonddev.craftoblo.scoreboard.SetupScorboard;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class HealPotion extends AbilityObject {

    @Override
    public void play(int button, Player p) {
        User user = DataManager.getUser(p);
        int ammount = getItemConfig(p).getInt("AMOUNT");
        if (ammount <= 0) {
            AbilityObject abObj = new EmptyHealPotion();
            p.getInventory().setItem(button, abObj.getItem(p));
            abObj.play(button, p);
            return;
        }
        if (ammount > 64) {
            getItemConfig(p).set("AMOUNT", 64);
            ammount = 64;
        }
        if (p.getHealth() == (p.getHealthScale())) {
            user.sendMessageAsSelf("I don't need that right now.");
        } else {
            p.setHealth(p.getHealthScale());
            if (p.getInventory().getItem(button).getAmount() - 1 == 0) {
                p.getInventory().setItem(button, new EmptyHealPotion().getItem(p));
            } else {
                p.getInventory().getItem(button).setAmount(ammount - 1);
                getItemConfig(p).set("AMOUNT", ammount - 1);
            }
            user.sendMessageAsSelf("All patched up!");
            SetupScorboard.set(p);
        }
    }

    @Override
    public String getName() {
        return "HEALTH_POTION";
    }

    @Override
    public void onCooldown(int button, Player p) {
        play(button, p);
    }

    @Override
    public ItemStack getItem(Player p) {
        User user = DataManager.getUser(p);
        if (!getItemConfig(p).contains("AMOUNT"))
            getItemConfig(p).set("AMOUNT", 5);
        int ammount = getItemConfig(p).getInt("AMOUNT", 5);
        if (ammount <= 0) {
            ammount = 1;
        }
        if (ammount > 64) {
            getItemConfig(p).set("AMOUNT", 64);
            ammount = 64;
        }
        return new ItemBuilder(Material.POTION)
                .displayName(ChatColor.RED + "Health Potion")
                .itemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS)
                .amount(ammount)
                .build();
    }

    @Override
    public int getUnlockLevel(Player p) {
        return 0;
    }

    @Override
    public ClassType getAllowedClass() {
        return ClassType.ALL;
    }

}
