package me.diamonddev.craftoblo.abilitys;

import me.diamonddev.craftoblo.camera.CameraManager;
import me.diamonddev.craftoblo.classes.ClassType;
import me.diamonddev.craftoblo.playerdata.ActionBar;
import me.diamonddev.craftoblo.playerdata.DataManager;
import me.diamonddev.craftoblo.playerdata.User;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class NearExplode extends AbilityObject {

    @Override
    public void play(int button, Player p) {
        User user = DataManager.getUser(p);
        if (user.getOrb() < 25) {
            user.sendMessageAsSelf("Not enough " + user.getCurrentClass().getOrbName());
            return;
        }
        Location loc = CameraManager.getManager().getArmorStand(p).getLocation();
        loc.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, loc, 20);
        user.setOrb(user.getOrb() - 25);
        AbilityCooldown.cooldown(p, button, 20);
    }

    @Override
    public void onCooldown(int button, Player p) {
        ActionBar.sendAction(p, "&cExplode is not ready yet!", true);
    }

    @Override
    public String getName() {
        return "NEAR_EXPLODE";
    }

    @Override
    public ItemStack getItem(Player p) {
        ItemStack abilityItem = new ItemStack(Material.TNT, 1);
        ItemMeta im = abilityItem.getItemMeta();
        im.setDisplayName(ChatColor.GRAY + "Explode");
        im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS);
        abilityItem.setItemMeta(im);
        return abilityItem;
    }

    @Override
    public int getUnlockLevel(Player p) {
        return 10;
    }

    @Override
    public ClassType getAllowedClass() {
        return ClassType.MONK;
    }

}
