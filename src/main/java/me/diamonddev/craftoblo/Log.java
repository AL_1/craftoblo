package me.diamonddev.craftoblo;

import org.bukkit.Bukkit;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {
    private static final Logger log = Bukkit.getServer().getLogger();
    private static final String prefix = "[" + Craftoblo.gi().getDescription().getName() + "] ";

    public static void info(Object obj) {
        log.info(prefix + obj.toString());
    }

    public static void warning(Object obj) {
        log.warning(prefix + obj.toString());
    }

    public static void debug(Level level, Object obj) {
        boolean debug = Craftoblo.gi().getConfig().getBoolean("Options.Debug");
        if (debug)
            log.log(level, prefix + obj.toString());
    }
}
