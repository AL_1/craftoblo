package me.diamonddev.craftoblo.menus;

import com.coalesce.gui.PlayerGui;
import me.diamonddev.craftoblo.Craftoblo;
import me.diamonddev.craftoblo.abilitys.AbilityObject;
import me.diamonddev.craftoblo.camera.CameraManager;
import me.diamonddev.craftoblo.classes.ClassType;
import me.diamonddev.craftoblo.playerdata.DataManager;
import me.diamonddev.craftoblo.playerdata.User;
import me.diamonddev.craftoblo.scoreboard.SetupScorboard;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;

public class ClassSelection extends PlayerGui {

    public ClassSelection() {
        super(Craftoblo.gi(), 9, ChatColor.RED + "" + ChatColor.UNDERLINE + "Class Selector");

        int count = 1;
        for (ClassType classType : ClassType.getRealClasses()) {
            setItem(count, player -> classType.getIcon(),
                    (inventoryClickEvent) -> {
                        Player player = (Player) inventoryClickEvent.getWhoClicked();
                        User user = DataManager.getUser(player);
                        classType.onSelect(player);
                        player.sendMessage(ChatColor.AQUA + "Your class was set to " + classType.getFullName(ChatColor.BOLD) + ChatColor.AQUA + ".");
                        user.setCurrentClass(classType);
                        PlayerInventory pinv = player.getInventory();
                        pinv.clear();
                        List<AbilityObject> abilityObject = user.getEquippedAbilities();
                        for (int i = 0; i < abilityObject.size(); i++)
                            user.getPlayer().getInventory().setItem(i, abilityObject.get(i).getItem(user.getPlayer()));
                        player.closeInventory();
                        SetupScorboard.set(player);
                        CameraManager.getManager().detach(player);
                        CameraManager.getManager().attach(player, 5, 5, 5);
                    });
            count++;
        }


    }
}
