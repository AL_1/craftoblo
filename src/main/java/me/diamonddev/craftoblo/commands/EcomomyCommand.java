package me.diamonddev.craftoblo.commands;

import me.diamonddev.craftoblo.playerdata.DataManager;
import me.diamonddev.craftoblo.playerdata.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class EcomomyCommand implements CommandExecutor {

    public int maxInt = 2147483647;

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("craftoblo.economy") || !sender.isOp()) {
            sender.sendMessage(ChatColor.RED + "Sorry, you do not have permission to run this command!");
            return true;
        }
        if (args.length == 0) {
            sender.sendMessage("/economy <set|give|take> <*|Player|UUID> <ammount>");
        } else if (args.length == 3) {
            List<User> users = new ArrayList<>();
            int ammount = 0;
            try {
                ammount = Integer.valueOf(args[2]);
            } catch (Exception e) {
                sender.sendMessage(ChatColor.RED + "\"" + args[2] + "\" is an invalid integer!");
            }
            if (args[1].equalsIgnoreCase("*")) {
                for (Player p : Bukkit.getOnlinePlayers()) {
                    users.add(DataManager.getUser(p));
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Player not found!");
            }
            if (args[0].equalsIgnoreCase("set")) {
                for (User user : users) {
                    user.setGold(ammount);
                    sender.sendMessage(ChatColor.AQUA + "Set " + ChatColor.GOLD + ChatColor.ITALIC
                            + user.getPlayer().getName() + ChatColor.AQUA + "'s gold to " + ChatColor.GOLD
                            + "" + ChatColor.BOLD + ammount + ChatColor.AQUA + ".");
                }
            } else if (args[0].equalsIgnoreCase("give")) {
                for (User user : users) {
                    user.setGold(user.getGold() + ammount);
                    sender.sendMessage(ChatColor.AQUA + "Gave " + ChatColor.GOLD + ChatColor.ITALIC
                            + user.getPlayer().getName() + ChatColor.GOLD + " " + ChatColor.BOLD + ammount
                            + ChatColor.AQUA + " gold. (" + ChatColor.GOLD + user.getGold()
                            + ChatColor.AQUA + ")");
                }
            } else if (args[0].equalsIgnoreCase("take")) {
                for (User user : users) {
                    user.setGold(user.getGold() - ammount);
                    sender.sendMessage(ChatColor.AQUA + "Took " + ChatColor.GOLD + "" + ChatColor.BOLD + ammount
                            + ChatColor.AQUA + " gold from " + ChatColor.GOLD + ChatColor.ITALIC
                            + user.getPlayer().getName() + ChatColor.AQUA + ". (" + ChatColor.GOLD
                            + user.getGold() + ChatColor.AQUA + ")");
                }
            }
        }
        return true;
    }

}
