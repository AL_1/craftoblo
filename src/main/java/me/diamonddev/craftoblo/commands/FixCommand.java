package me.diamonddev.craftoblo.commands;

import me.diamonddev.craftoblo.abilitys.AbilityObject;
import me.diamonddev.craftoblo.camera.CameraManager;
import me.diamonddev.craftoblo.playerdata.DataManager;
import me.diamonddev.craftoblo.playerdata.User;
import me.diamonddev.craftoblo.scoreboard.SetupScorboard;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;

public class FixCommand implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You have nothing to be fixed");
            return true;
        }
        Player p = (Player) sender;
        PlayerInventory inv = p.getInventory();
        User user = DataManager.getUser(p);
        List<AbilityObject> abilityObject = user.getEquippedAbilities();

        if (args.length == 0 || args[0].equalsIgnoreCase("all") || args[0].equalsIgnoreCase("a")) {
            p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "(!)" + ChatColor.YELLOW + " Fixing player.");
            // Camera detach
            CameraManager.getManager().detach(p);

            // Reposition if needed
            if (!p.getLocation().getBlock().getType().equals(Material.AIR)
                    || !p.getEyeLocation().getBlock().getType().equals(Material.AIR)) {
                p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "(!)" + ChatColor.YELLOW
                        + " Invalid location! Sending you to spawn.");
                p.teleport(p.getLocation().getWorld().getSpawnLocation());
            }

            // inv setup
            inv.clear();
            inv.setHeldItemSlot(7);
            for (int i = 0; i < abilityObject.size(); i++)
                user.getPlayer().getInventory().setItem(i, abilityObject.get(i).getItem(user.getPlayer()));

            // Scorboard reset
            SetupScorboard.set(p);

            // Camera attach
            CameraManager.getManager().attach(p, 5, 5, 5);
        } else if (args[0].equalsIgnoreCase("location") || args[0].equalsIgnoreCase("l")) {
            p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "(!)" + ChatColor.YELLOW + " Fixing location.");
            CameraManager.getManager().detach(p);
            p.teleport(p.getLocation().getWorld().getSpawnLocation());
            CameraManager.getManager().attach(p, 5, 5, 5);
        } else if (args[0].equalsIgnoreCase("abilitys") || args[0].equalsIgnoreCase("items")
                || args[0].equalsIgnoreCase("ab") || args[0].equalsIgnoreCase("i")) {
            p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "(!)" + ChatColor.YELLOW + " Fixing items/abilitys.");
            inv.clear();
            inv.setHeldItemSlot(7);
            for (int i = 0; i < abilityObject.size(); i++)
                user.getPlayer().getInventory().setItem(i, abilityObject.get(i).getItem(user.getPlayer()));

        } else if (args[0].equalsIgnoreCase("scorboard") || args[0].equalsIgnoreCase("s")) {
            p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "(!)" + ChatColor.YELLOW + " Fixing scoreboard.");
            SetupScorboard.set(p);

        } else {
            p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "(!)" + ChatColor.GRAY + "Unknown syntax!");
        }

        return true;
    }
}
