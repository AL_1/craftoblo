package me.diamonddev.craftoblo.commands;

import me.diamonddev.craftoblo.camera.CameraManager;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CameraCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player))
            return true;
        Player p = (Player) sender;
        if (!p.hasPermission("craftoblo.debug") || !p.isOp()) {
            p.sendMessage(ChatColor.RED
                    + "Sorry this command is not finished or is a debug command and you cannot use it yet. If you belive this is an error please contact an administator.");
            return true;
        }
        if (args.length == 2) {
            Location loc = CameraManager.getManager().getArmorStand(p).getLocation();
            p.sendMessage("X: " + loc.getX());
            p.sendMessage("Y: " + loc.getY());
            p.sendMessage("Z: " + loc.getZ());
            p.sendMessage("K: " + loc.getYaw());
            p.sendMessage("P: " + loc.getPitch());
            return true;
        }
        if (CameraManager.getManager().isAttatched(p)) {
            CameraManager.getManager().detach(p);
        } else {
            CameraManager.getManager().attach(p, 5, 5, 5);
        }
        return true;
    }

}
