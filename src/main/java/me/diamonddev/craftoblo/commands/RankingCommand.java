package me.diamonddev.craftoblo.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class RankingCommand implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage("This command is broken");
        return true;
    }


	/*public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
			sender.sendMessage("/ranking <gold|level|overall>");
		} else {
			if (args[0].equalsIgnoreCase("gold")) {
				String prefixFormat = "&7&m+-----------------&7[&aRanking &8// &6Gold&7]&m-----------------+";
				String totalFormat = "&bServer Total&r &8&m-&r &6&l%s";
				String rootFormat = "&8%s) &b%s&r &8&m-&r &6&l%s";
				String suffixFormat = "&7&m+-------------------&7[&aPage &d%s&8/&d%s&7]&m-------------------+";
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefixFormat));
				sender.sendMessage(
						ChatColor.translateAlternateColorCodes('&', String.format(totalFormat, getTotalGold())));
				if (args.length == 1) {
					List<Entry<String, Integer>> sortedValues = getSortedGoldValues();
					int count = 0;
					for (Entry<String, Integer> entry : sortedValues) {
						count++;
						if (count >= 1 && count <= 5)
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
									String.format(rootFormat, count, entry.getKey(), entry.getValue())));
					}
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', String.format(suffixFormat, 1,
							String.valueOf(Math.round((float) Math.ceil(((double) count / 5)))))));
				} else if (args.length == 2) {
					int page = 1;
					try {
						page = Integer.parseInt(args[1]);
					} catch (Exception ignored) {
					}
					int start = (page * 5) - 4;
					int end = page * 5;
					List<Entry<String, Integer>> sortedValues = getSortedGoldValues();
					int count = 0;
					for (Entry<String, Integer> entry : sortedValues) {
						count++;
						if (count >= start && count <= end)
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
									String.format(rootFormat, count, entry.getKey(), entry.getValue())));
					}
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', String.format(suffixFormat, page,
							String.valueOf(Math.round((float) Math.ceil(((double) count / 5)))))));
				}
			} else if (args[0].equalsIgnoreCase("level")) {
				String prefixFormat = "&7&m+-----------------&7[&aRanking &8// &bLevel&7]&m-----------------+";
				String rootFormat = "&8%s) &b%s&r &8&m-&r &6&l%s";
				String suffixFormat = "&7&m+-------------------&7[&aPage &d%s&8/&d%s&7]&m-------------------+";
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefixFormat));
				if (args.length == 1) {
					List<Entry<String, Integer>> sortedValues = getSortedLevelValues();
					int count = 0;
					for (Entry<String, Integer> entry : sortedValues) {
						count++;
						if (count >= 1 && count <= 5)
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
									String.format(rootFormat, count, entry.getKey(), entry.getValue())));
					}
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', String.format(suffixFormat, 1,
							String.valueOf(Math.round((float) Math.ceil(((double) count / 5)))))));
				} else if (args.length == 2) {
					int page = 1;
					try {
						page = Integer.parseInt(args[1]);
					} catch (Exception ignored) {
					}
					int start = (page * 5) - 4;
					int end = page * 5;
					List<Entry<String, Integer>> sortedValues = getSortedLevelValues();
					int count = 0;
					for (Entry<String, Integer> entry : sortedValues) {
						count++;
						if (count >= start && count <= end)
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
									String.format(rootFormat, count, entry.getKey(), entry.getValue())));
					}
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', String.format(suffixFormat, page,
							String.valueOf(Math.round((float) Math.ceil(((double) count / 5)))))));
				}
			}
		}
		return true;
	}

	private int getTotalGold() {
		List<Entry<String, Integer>> sortedValues = getSortedGoldValues();
		int totalGold = 0;
		for (Entry<String, Integer> entry : sortedValues) {
			totalGold = totalGold + entry.getValue();
		}
		return totalGold;
	}

	private static List<Entry<String, Integer>> getSortedGoldValues() {
		List<PlayerData> dataList = DataManager.getAllPlayerData();
		HashMap<String, Integer> gold = new HashMap<>();
		for (PlayerData data : dataList) {
			gold.put(data.getData().getString("Username"), data.getGold());
		}
        return entriesSortedByValues(gold);
	}

	private static List<Entry<String, Integer>> getSortedLevelValues() {
		List<PlayerData> dataList = DataManager.getAllPlayerData();
		HashMap<String, Integer> level = new HashMap<>();
		for (PlayerData data : dataList) {
			String playerClass = "None";
			if (data.getData().contains("Class"))
				playerClass = data.getPlayerClass();
			level.put(data.getData().getString("Username"),
					(int) Math.floor(data.getLevel(ClassType.valueOf(playerClass.toUpperCase()))));
		}
        return entriesSortedByValues(level);
	}

	private static <K, V extends Comparable<? super V>> List<Entry<K, V>> entriesSortedByValues(Map<K, V> map) {

		List<Entry<K, V>> sortedEntries = new ArrayList<>(map.entrySet());

		sortedEntries.sort((e1, e2) -> e2.getValue().compareTo(e1.getValue()));

		return sortedEntries;
	}*/

}
