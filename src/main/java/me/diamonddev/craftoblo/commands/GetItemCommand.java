package me.diamonddev.craftoblo.commands;

import me.diamonddev.craftoblo.Craftoblo;
import me.diamonddev.craftoblo.items.Item;
import me.diamonddev.craftoblo.items.ItemManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GetItemCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return true;
        if (!sender.hasPermission("craftoblo.debug") || !sender.isOp()) {
            sender.sendMessage(ChatColor.RED
                    + "Sorry this command is not finished or is a debug command and you cannot use it yet. If you belive this is an error please contact an administator.");
            return true;
        }
        if (args.length == 1) {
            ItemManager manager = Craftoblo.gi().getItemManager();
            Item item = manager.getItem(args[0]);
            if (item == null) {
                sender.sendMessage(ChatColor.RED + "Item not found!");
                return true;
            }
            ((Player) sender).getInventory().addItem(manager.getItemStack(item));
        } else {
            sender.sendMessage("/getItem <Item ID>");
        }
        return true;
    }
}
