package me.diamonddev.craftoblo.commands;

import me.diamonddev.craftoblo.actions.LevelUp;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LevelCommand implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Only players have levels!");
            return true;
        }
        Player p = (Player) sender;
        if (!p.hasPermission("craftoblo.debug") || !p.isOp()) {
            p.sendMessage(ChatColor.RED
                    + "Sorry this command is for administators you cannot use it. If you belive this is an error please contact an administator.");
            return true;
        }
        LevelUp.play(p, false);
        return true;
    }
}
