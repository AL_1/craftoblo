package me.diamonddev.craftoblo.commands;

import me.diamonddev.craftoblo.playerdata.DataManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GoldCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            if (sender instanceof Player) {
                Player p = (Player) sender;
                p.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Gold: " + ChatColor.AQUA
                        + DataManager.getUser(p).getGold());
            } else {
                sender.sendMessage("/gold <player|uuid>");
            }
        } else if (args.length == 1) {
            Player p = null;
            try {
                p = Bukkit.getPlayer(args[0]);
            } catch (Exception e) {
                sender.sendMessage(ChatColor.RED + "Player not found!");
                return true;
            }
            sender.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + p.getDisplayName() + "'s Gold: " + ChatColor.AQUA
                    + DataManager.getUser(p).getGold());
        } else {
            sender.sendMessage("/gold [player|uuid]");
        }
        return true;
    }

}
