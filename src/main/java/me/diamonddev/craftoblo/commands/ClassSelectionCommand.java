package me.diamonddev.craftoblo.commands;

import me.diamonddev.craftoblo.menus.ClassSelection;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClassSelectionCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You cannot pick a class, sorry :P");
            return true;
        }
        Player p = (Player) sender;
        ClassSelection menu = new ClassSelection();
        menu.open(p);
        return true;
    }

}
