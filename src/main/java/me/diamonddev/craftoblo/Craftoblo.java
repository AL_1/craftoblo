package me.diamonddev.craftoblo;

import com.coalesce.plugin.CoPlugin;
import lombok.Getter;
import lombok.Setter;
import me.diamonddev.craftoblo.animation.AnimationManager;
import me.diamonddev.craftoblo.camera.CameraManager;
import me.diamonddev.craftoblo.items.ItemManager;
import me.diamonddev.craftoblo.playerdata.DataManager;
import me.diamonddev.craftoblo.playerdata.User;
import me.diamonddev.craftoblo.scoreboard.SetupScorboard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class Craftoblo extends CoPlugin {

    @Getter
    private final AnimationManager animationManager;
    @Getter
    @Setter
    private ItemManager itemManager;

    public Craftoblo() {
        this.animationManager = new AnimationManager();
    }

    public static Craftoblo gi() {
        return (Craftoblo) Bukkit.getPluginManager().getPlugin("Craftoblo");

    }

    public static ItemStack changeLoreLine(ItemStack stack, int line, String newValue) {
        ItemMeta meta = stack.getItemMeta();
        List<String> lore = meta.getLore();
        while (lore.size() < line)
            lore.add(" ");
        lore.set(line, newValue);
        meta.setLore(lore);
        stack.setItemMeta(meta);
        return stack;
    }

    @Override
    public void onPluginEnable() throws Exception {
        MainConfigManager.checkConfig();
        FileLoader.CheckFiles();
        Registers.registerAll();
        DataManager.afterReload();
        for (Player p : getServer().getOnlinePlayers()) {
            p.sendMessage("§b§oA new §a§oCraftoblo §b§oupdate has been completed!");
        }
    }

    @Override
    public void onPluginDisable() throws Exception {
        for (User user : DataManager.getAllOnlineUsers()) {
            CameraManager.getManager().detach(user.getPlayer());
            SetupScorboard.bossBars.get(user.getPlayer().getUniqueId()).setVisible(false);
            user.save();
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage(ChatColor.RED + "This command is broken, please contact an administrator about this problem."
                + ChatColor.GRAY + " (ER: 0001)");
        return true;
    }

}
